package com.bambr.sweet.integration;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.tinkerpop.blueprints.Vertex;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Daniil on 30.07.2015..
 */
public class UrlGrabberBolt implements IRichBolt {
    String name;
    private OutputCollector collector;
    private TopologyContext context;
    private Map conf;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.context = context;
        this.conf = stormConf;
        this.name = context.getThisComponentId();
    }

    @Override
    public void execute(Tuple input) {
        String url = input.getString(0);
        try {
            Document document = Jsoup.connect(url).get();
            collector.emit(new Values(url, document));
            collector.ack(input);
        } catch (IOException e) {
            e.printStackTrace();
            collector.fail(input);
        }

    }

    @Override
    public void cleanup() {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url", "content"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
