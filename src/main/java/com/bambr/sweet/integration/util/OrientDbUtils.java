package com.bambr.sweet.integration.util;

import com.bambr.sweet.integration.App;
import com.bambr.sweet.integration.FetchStatus;
import com.orientechnologies.common.concur.ONeedRetryException;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import java.util.Map;

/**
 * Created by Daniil on 30.07.2015..
 */
public class OrientDbUtils {
    public boolean updateVertex(OrientGraph graph, Vertex vertex, Map<String, Object> values, Long maxRetries) {
        boolean result = false;
        synchronized (graph) {
            for (int i = 0; i < maxRetries; i++) {
                try {
                    graph.makeActive();
                    graph.begin();
                    for (Map.Entry<String, Object> e : values.entrySet()) {
                        vertex.setProperty(e.getKey(), e.getValue());
                    }
                    graph.commit();
                    result = true;
                } catch (ONeedRetryException e) {
                }
            }
        }
        return result;
    }

    public void createVertexIfNotExists(OrientGraph graph, String vertexClass, Map<String, Object> values, Long maxRetries) {
        synchronized (graph) {
            Vertex vertex = null;
            try {
                for (int i = 0; i < maxRetries; i++) {
                    try {
                        graph.makeActive();
                        graph.begin();
                        vertex = graph.addVertex("class:" + vertexClass, "fetchStatus", FetchStatus.NEW, "fetchRetries", 0, "url", values.get("url"));
                        graph.commit();
                    } catch (ONeedRetryException e) {

                    }
                }
            } catch (ORecordDuplicatedException e) {

            }
        }
    }

    public static Vertex getVertexByUrl(String url) {
        OCommandSQL findVertexByUrlCommand  = new OCommandSQL("SELECT from DataNode where url='"+url+"'");
        Vertex vertex = null;
        Iterable<Vertex> foundVertices = (Iterable<Vertex>) App.factory.getTx().command(findVertexByUrlCommand).execute();
        if(foundVertices.iterator().hasNext()) {
            vertex = foundVertices.iterator().next();
        }
        return vertex;
    }
}
