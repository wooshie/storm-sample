package com.bambr.sweet.integration;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import org.apache.storm.jetty.util.ConcurrentHashSet;

import java.util.ArrayList;

/**
 * Created by Daniil on 30.07.2015..
 */
public class App {
    public static OrientGraphFactory factory;

    public static void main(String... args) throws InterruptedException {
        factory = new OrientGraphFactory("remote:localhost/sweet","username", "password").setupPool(1,128);
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("url-reader", new OrientDbSprout());
        builder.setBolt("url-grabber", new UrlGrabberBolt()).shuffleGrouping("url-reader");
        builder.setBolt("content-parser", new ContentParserBolt()).fieldsGrouping("url-grabber", new Fields("url", "content"));

        Config config = new Config();
        config.setDebug(false);
        config.put("nodeClass", "DataNode");
        config.put("cssSelectors", "a.am###a.a1###a.navi###[id^=mtd]###[id^=ahc]");
        config.put("baseUrl", "http://www.ss.lv");
        config.put(Config.TOPOLOGY_MAX_SPOUT_PENDING, 2);
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("DataGrabber topology", config, builder.createTopology());
        Thread.sleep(10000);
        cluster.shutdown();
        factory.close();
    }
}
