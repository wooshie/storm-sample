package com.bambr.sweet.integration;

/**
 * Created by Daniil on 30.07.2015..
 */
public interface FetchStatus {
    public static final String NEW = "new";
    public static final String FETCHING = "fetching";
    public static final String RETRIEVED = "retrieved";
    public static final String FAILED = "failed";
    public static final String RAW = "raw";
}
