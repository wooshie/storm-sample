package com.bambr.sweet.integration;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import com.bambr.sweet.integration.util.OrientDbUtils;
import com.orientechnologies.orient.core.command.OCommandRequest;
import com.orientechnologies.orient.core.db.OPartitionedDatabasePoolFactory;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.Vertex;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.net.URI;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * Created by Daniil on 30.07.2015..
 */
public class ContentParserBolt implements IRichBolt {
    private OutputCollector collector;
    private TopologyContext context;
    private Map conf;
    private String name;
    private List<String> cssSelectors;
    private String nodeClass;
    private OrientDbUtils utils;
    private String baseUrl;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.context = context;
        this.conf = stormConf;
        this.name = context.getThisComponentId();
        this.cssSelectors = Arrays.asList(((String) conf.get("cssSelectors")).split("###"));
        this.baseUrl = (String)conf.get("baseUrl");
        this.nodeClass = (String)conf.get("nodeClass");
        this.utils = new OrientDbUtils();
    }

    @Override
    public void execute(Tuple input) {
        //Input should be having form of: "url","content"
        String url = input.getString(0);
        Document document = (Document) input.getValue(1);
        System.out.println("Document: " + document.html());
        Vertex vertex = OrientDbUtils.getVertexByUrl(url);
        if(vertex == null) {
            System.err.println("Not found vertex for url: "+url);
            return;
        }
        System.out.println("CSS Selectors");
        cssSelectors.forEach(System.out::println);
        cssSelectors.forEach(selector -> storeChildren(getChildren(document, selector)));
        if (canSave(url)) {
            //Store content
            Map<String, Object> values = new HashMap<String, Object>() {{
                put("content", document.html());
                put("fetchStatus", FetchStatus.RAW);
            }};
            utils.updateVertex(App.factory.getTx(), vertex, values, 5L);
        }
    }



    protected void storeChildren(List<String> children) {
        System.out.println("children.size(): "+children.size());
        for (String url : children) {
            System.out.println("Processing url: "+url);
            Map<String, Object> values = new HashMap<String, Object>() {{
                put("fetchStatus", FetchStatus.NEW);
                put("url", url.startsWith("/") ? baseUrl+url : url);
                put("fetchRetries", 0);
            }};
            utils.createVertexIfNotExists(App.factory.getTx(), nodeClass, values, 5L);
        }
    }

    protected boolean canSave(String url) {
        try {
            URI uri = URI.create(url);
            String path = uri.getPath();
            String[] pathParts = path.split("\\/");
            String fileName = pathParts[pathParts.length - 1];
            return (fileName.endsWith("html") && !fileName.startsWith("page"));
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

    private List<String> getChildren(Document doc, String cssQuery) {
        return doc
                .select(cssQuery)
                .stream()
                .filter(hrefFilterPredicate())
                .map(filtered -> filtered.attr("href"))
                .collect(Collectors.toList());
    }

    protected Predicate<? super Element> hrefFilterPredicate() {
        return (Element raw) -> (raw.attr("href").startsWith("/ru") || raw.attr("href").startsWith("/msg")) && !raw.attr("href").contains("rss") && (raw.attr("href").split("\\/").length - 1 > 2);
    }

}
