package com.bambr.sweet.integration;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import com.bambr.sweet.integration.util.OrientDbUtils;
import com.orientechnologies.common.concur.ONeedRetryException;
import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.db.ODatabase;
import com.orientechnologies.orient.core.db.record.ORecordOperation;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.sql.query.OLiveQuery;
import com.orientechnologies.orient.core.sql.query.OLiveResultListener;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Daniil on 30.07.2015..
 */
public class OrientDbSprout implements IRichSpout {
    private Map conf;
    private TopologyContext context;
    private SpoutOutputCollector collector;
    private OrientDbUtils utils;
    private volatile String token;
    private volatile ConcurrentSkipListSet<ODocument> liveQueryResults = new ConcurrentSkipListSet<>();
    private volatile Thread liveQueryThread;

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        System.out.println("OrientDbSprout opened");
        this.conf = conf;
        this.context = context;
        this.collector = collector;
        //open orientdb connection
        this.utils = new OrientDbUtils();
        if (liveQueryThread == null) {
            liveQueryThread = new Thread(new LiveQueryRunnable());
        }
        if (liveQueryThread != null && !liveQueryThread.isAlive()) {
            liveQueryThread.start();
        }
    }

    @Override
    public void close() {
        System.out.println("OrientDbSprout closed");
        //close orientdb connection
        App.factory.getNoTx().command(new OCommandSQL("live unsubscribe " + token)).execute();
        liveQueryThread.interrupt();
    }

    @Override
    public void activate() {
        System.out.println("OrientDbSprout activated");
    }

    @Override
    public void deactivate() {
        System.out.println("OrientDbSprout de-activated");
    }

    @Override
    public void nextTuple() {
        System.out.println("OrientDbSprout nextTuple called");
        System.out.println("OrientDbSprout liveQueryResults.size: " + liveQueryResults.size());
        if (liveQueryResults.size() > 0) {
            Iterator<ODocument> liveQueryResultsIterator = liveQueryResults.iterator();
            while (liveQueryResultsIterator.hasNext()) {
                ODocument liveQueryResult = liveQueryResultsIterator.next();
                String url = liveQueryResult.field("url");
                liveQueryResult.field("fetchStatus", FetchStatus.FETCHING);
                liveQueryResult.save();
                collector.emit(new Values(url, url));
                liveQueryResultsIterator.remove();

            }
        } else {
            Utils.sleep(50);
        }
    }

    @Override
    public void ack(Object msgId) {
        //Mark record completed
        Map<String, Object> values = new HashMap<String, Object>() {{
            put("fetchStatus", FetchStatus.RETRIEVED);
        }};
        String url = (String) msgId;
        Vertex vertex = OrientDbUtils.getVertexByUrl(url);
        if (url != null) {
            utils.updateVertex(App.factory.getTx(), vertex, values, 5L);
        } else {
            System.out.println("Couldn't find vertex for url: " + url);
            System.exit(-100);
        }
    }


    @Override
    public void fail(Object msgId) {
        //Mark record failed
        Vertex vertex = (Vertex) msgId;
        Map<String, Object> values = new HashMap<String, Object>() {{
            put("fetchStatus", FetchStatus.FAILED);
            put("fetchRetries", Integer.valueOf(vertex.getProperty("fetchRetries")) + 1);
        }};
        utils.updateVertex(App.factory.getTx(), (Vertex) msgId, values, 5L);
    }

    private class LiveQueryRunnable implements Runnable {
        @Override
        public void run() {
            ODatabase<ORecord> database = App.factory.getDatabase();
            System.out.println("LiveQueryRunnable: database -> "+database);
            OLiveResultListener listener = (iLiveToken, iOp) -> {
                ODocument result = (ODocument) iOp.getRecord();
                liveQueryResults.add(result);
            };
            System.out.println("LiveQueryRunnable: listener -> "+listener);
            List<ODocument> liveResult = database.query(new OLiveQuery<ODocument>("select from DataNode where (fetchStatus='new' or fetchStatus='failed') and fetchRetries < 3", listener));
            System.out.println("LiveQueryRunnable: liveResult -> "+liveResult);
            if(liveResult != null) {
                liveResult.forEach(System.out::println);
            }
            token = liveResult.get(0).field("token");
        }
    }

}
