This is sample utilizing Apache Storm and OrientDb for purpose of scrapping some web content.
    Fiddle with App.java changing configuration as you find necessary. Face the consequences though :)
    
Configuration:

    App.java
        "remote:localhost/sweet","username", "password" - replace "username" and "password" with your credentials configured in OrientDb
        "nodeClass", "DataNode" - don't change it unless you create your own class in OrientDb data structure. If you did it -  watch the fields throughout the code though :)
        "cssSelectors", "a.am###a.a1###a.navi###[id^=mtd]###[id^=ahc]" - feel free to change it according to your needs, as long as it keeps digging through the web site structure. If you modify it, make sure modify also "hrefFilterPredicate" in ContentParserBolt according to the content lookup logic. Note that these CSS selectors are Jsoup one separated by ###
        "baseUrl", "http://www.ss.lv" - base url for site to grab (NB! it is not the start url), changing it most certainly will require modification of cssCeletors part and predicate mentioned above.
         
     Database
        You should have record of class "DataNode" (or the one you've chosen as "nodeClass") with valid "url" field, "fetchStatus": "new" and "fetchRetries": 0
        Example/default: 
            "url": "https://www.ss.lv/ru/", "fetchStatus": "new", "fetchRetries": 0, "content": "nothing"
        This is what should trigger the processing.
  
         

Platform:
    You need to have Java 8 installed in order to run this sample

Database:
    You need to run OrientDb 2.1 or higher.
    Make sure you enabled live queries in "config/orientdb-server-config.xml"
    create database named "sweet" of storage type "remote"
    import "sweet(2)" file from "dump" folder
    
Apache Storm:
    Get the latest version, unpack it somewhere
    
Building:
    Usual Gradle build. If you don't have Gradle installed, run either "gradlew" or "gradlew.bat" script depending on you platform.
    Example: 
   
        *nix:
            ./gradlew shadowJar
   
        Windows:
            gradlew.bat shadowJar
    As a result of successful build you should have "build" folder appearing in your project and there in "libs" sub-folder you will find resulting jar.
    
Running (console only):
    Change working directory to "bin" folder of your Apache Storm and there execute following line:
        
        *nix:
            ./storm jar /opt/integration-1.0-SNAPSHOT-all.jar com.bambr.sweet.integration.App
            
        Windows:
            storm.cmd jar /opt/integration-1.0-SNAPSHOT-all.jar com.bambr.sweet.integration.App
            
The logic of this topology is that OrientDbSprout is utilizing live query to fetch some records with certain conditions and then emit information from them to UrlGrabberBolt and further to ContentParserBolt, which can insert to database some new records to process.

Output grabbing:

    *nix:
        script yourfilename.txt
        ./storm jar /opt/integration-1.0-SNAPSHOT-all.jar com.bambr.sweet.integration.App
        exit
        less yourfilename.txt
    Note: you might want to stop execution of topology by pressing Ctrl+C (then you go on with "exit" command)
